package com.dgut;

import com.dgut.bean.*;
import com.dgut.mapper.AdminMapper;
import com.dgut.mapper.DeclareMapper;
import com.dgut.mapper.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//与系统无关，一些方法测试
public class MyTest {

    @Test
    public void test9() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        DeclareMapper declareMapper = sqlSession.getMapper(DeclareMapper.class);
        List<DeclareAndUser> declareAndUsers = declareMapper.findAllByAdmin();
        for(DeclareAndUser declareAndUser : declareAndUsers)
            System.out.println(declareAndUser);
    }

    /**
     * 模糊查询
     */
    @Test
    public void test8() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        DeclareMapper declareMapper = sqlSession.getMapper(DeclareMapper.class);
        List<Declare> declares = declareMapper.searchPerson(1, "2020-06-25");
        for (Declare declare : declares) {
            System.out.println(declare);
        }
    }

    /**
     * 数据库反应给前端
     */
    @Test
    public void test7() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        DeclareMapper declareMapper = sqlSession.getMapper(DeclareMapper.class);
        List<Declare> declares = declareMapper.findAll();
//        for(Declare declare : declares){
//            System.out.println(declare);
//        }
        List<FrontDeclare> frontDeclares = new ArrayList<>();
        for (Declare declare : declares) {
            FrontDeclare frontDeclare = new FrontDeclare();
            List list = new ArrayList();
            frontDeclare.setDid(declare.getDid());
            frontDeclare.setUid(declare.getUid());
            frontDeclare.setTemperature(declare.getTemperature());
            frontDeclare.setIsDiscomfort(declare.getIsDiscomfort());
            frontDeclare.setIsObservation(declare.getIsObservation());
            frontDeclare.setObservationType(declare.getObservationType());
            frontDeclare.setObservationDate(declare.getObservationDate());
            frontDeclare.setWhereabouts(declare.getWhereabouts());
            frontDeclare.setDeclareDate(declare.getDeclareDate());
            if (frontDeclare.getIsDiscomfort().equals("是")) {

                if (declare.getCough() != null && declare.getCough().equals("是"))
                    list.add("咳嗽");
                if (declare.getWeak() != null && declare.getWeak().equals("是"))
                    list.add("乏力");
                if (declare.getDizzy() != null && declare.getDizzy().equals("是"))
                    list.add("头晕");
                if (declare.getDiarrhea() != null && declare.getDiarrhea().equals("是"))
                    list.add("腹泻");
            }
            frontDeclare.setDiscomfort(list);
            frontDeclares.add(frontDeclare);
        }
        for (FrontDeclare displayFrontDeclare : frontDeclares)
            System.out.println(displayFrontDeclare);
    }

    /**
     * 疫情表前端写入数据库
     *
     * @throws IOException
     */
    @Test
    public void test6() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        DeclareMapper declareMapper = sqlSession.getMapper(DeclareMapper.class);
//        List<Declare> declares = declareMapper.findAll();
//        for(Declare declare : declares){
//            System.out.println(declare);
//        }
        List list = new ArrayList();
        list.add("咳嗽");
        list.add("头晕");
//        System.out.println(list);
        FrontDeclare frontDeclare = new FrontDeclare();
        frontDeclare.setUid(1);
        frontDeclare.setTemperature("体温37.3度以下");
        frontDeclare.setIsDiscomfort("是");
        frontDeclare.setDiscomfort(list);
        frontDeclare.setIsObservation(null);
        frontDeclare.setObservationType("集中隔离");
        frontDeclare.setObservationDate(null);
        frontDeclare.setWhereabouts(null);
        frontDeclare.setDeclareDate(new Date());
        System.out.println(frontDeclare);

        Declare declare = new Declare();
        declare.setDid(frontDeclare.getDid());
        declare.setUid(frontDeclare.getUid());
        declare.setTemperature(frontDeclare.getTemperature());
        declare.setIsDiscomfort(frontDeclare.getIsDiscomfort());
        declare.setIsObservation(frontDeclare.getIsObservation());
        declare.setObservationType(frontDeclare.getObservationType());
        declare.setObservationDate(frontDeclare.getObservationDate());
        declare.setWhereabouts(frontDeclare.getWhereabouts());
        declare.setDeclareDate(frontDeclare.getDeclareDate());
        if (declare.getIsDiscomfort().equals("是")) {
            for (Object item : frontDeclare.getDiscomfort()) {
                if (item.equals("咳嗽"))
                    declare.setCough("是");
                else if (item.equals("乏力"))
                    declare.setWeak("是");
                else if (item.equals("头晕"))
                    declare.setDizzy("是");
                else if (item.equals("腹泻"))
                    declare.setDiarrhea("是");
            }
        }
        declareMapper.addDeclare(declare);


//        declareMapper.addDeclare(frontDeclare);
    }


    @Test
    public void test4() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        User user = userMapper.findUserById(6);
        ;
        user.setBirthday(new Date());
        userMapper.updateUserInformation(user);
        System.out.println(user);
    }

    @Test
    public void test5() throws ParseException {
//        获取本地时间
//        String dateString = formatter.format(currentTime);
//        ParsePosition pos = new ParsePosition(8);
//        Date currentTime_2 = formatter.parse(dateString, pos);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = dateFormat.format(new Date());
        Date birthday = dateFormat.parse(dateString);
        System.out.println(dateString);
        System.out.println(birthday);
    }

    @Test
    public void test1() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        List<User> users = userMapper.findAll();
        for (User user : users) {
            System.out.println(user);
        }
    }


    @Test
    public void test3() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        String username = "1111";
        String password = "123";
        userMapper.addUser(username, password);
        User user = userMapper.findUserByUsername(username);
        System.out.println(user);
    }

    @Test
    public void test2() {
        String test = "This is test for string";
        System.out.println(test.indexOf("This")); //0
        System.out.println(test.indexOf("is")); //2
        System.out.println(test.indexOf("test")); //8
        System.out.println(test.indexOf("for")); //13
        System.out.println(test.indexOf("for string       "));//-1
        if (test.indexOf("This") != -1) {
            //"只要test.indexOf('This')返回的值不是-1说明test字符串中包含字符串'This',相反如果包含返回的值必定是-1"
            System.out.println("存在包含关系，因为返回的值不等于-1");
        } else {
            System.out.println("不存在包含关系，因为返回的值等于-1");
        }
        if (test.indexOf("this") != -1) {
            //"只要test.indexOf('this')返回的值不是-1说明test字符串中包含字符串'this',相反如果包含返回的值必定是-1"
            System.out.println("存在包含关系，因为返回的值不等于-1");
        } else {
            System.out.println("不存在包含关系，因为返回的值等于-1");
        }
        String test1 = "/WEB-INF/pages/login_test.jsp";
        System.out.println(test1.indexOf("login_test.jsp"));
    }
}
