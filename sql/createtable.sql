DROP database IF EXISTS `ssm_db`;
create database ssm_db
DEFAULT CHARACTER SET utf8
DEFAULT COLLATE utf8_general_ci;

use ssm_db;

# 创建数据表

#用户表
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
                        `uid` int NOT NULL AUTO_INCREMENT,
                        `username` varchar(16) NOT NULL Unique,
                        `password` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                        `idNumber` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL Unique,
                        `realName` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
                        `sex` enum('男','女') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
                        `birthday` date DEFAULT NULL,
                        `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
                        `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
                        `userImage` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
                        PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'test1', '123456', '440000199908229510','张三', '男', '2020-08-22', '13454123121', '广东东莞', null);
INSERT INTO `user` VALUES ('2', 'test2', '123456', '440000199905169520','李四', '女', '2020-05-16', '13454465875', '广东广州', null);
INSERT INTO `user` VALUES ('3', 'test3', '123456', '440000199906089530','王五', '男', '2020-06-08', '13434553457', '广东深圳', null);
INSERT INTO `user` VALUES ('4', 'test4', '123456', '440000199903099550','赵六', '男', '2020-03-09', '13458745475', '广东东莞', null);
INSERT INTO `user` VALUES ('5', 'test5', '123456', '440000199904189540','丁七', '女', '2020-04-18', '13458904564', '广东深圳', null);



/*管理员表*/
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
                         `aid` int NOT NULL AUTO_INCREMENT,
                         `adminName` varchar(16) NOT NULL,
                         `password` varchar(16) NOT NULL,
                         `adminImage` varchar(100) DEFAULT NULL,
                         PRIMARY KEY (`aid`),
                         UNIQUE KEY `adminName_inserst` (`adminName`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin1', '123456', '4.jpg');
INSERT INTO `admin` VALUES ('2', 'admin2', '123456', null);


/*疫情申报信息表（后面要改）*/


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `declare`
-- ----------------------------
DROP TABLE IF EXISTS `declare`;
CREATE TABLE `declare` (
                           `did` int NOT NULL AUTO_INCREMENT,
                           `uid` int NOT NULL,
                           `temperature` enum('体温37.3度以下','体温37.3度以上') CHARACTER SET gbk COLLATE gbk_chinese_ci DEFAULT NULL,
                           `isDiscomfort` enum('是','否') CHARACTER SET gbk COLLATE gbk_chinese_ci DEFAULT NULL,
                           `cough` enum('是','否') CHARACTER SET gbk COLLATE gbk_chinese_ci DEFAULT NULL,
                           `weak` enum('是','否') CHARACTER SET gbk COLLATE gbk_chinese_ci DEFAULT NULL,
                           `dizzy` enum('是','否') CHARACTER SET gbk COLLATE gbk_chinese_ci DEFAULT NULL,
                           `diarrhea` enum('是','否') CHARACTER SET gbk COLLATE gbk_chinese_ci DEFAULT NULL,
                           `isObservation` enum('是','否') CHARACTER SET gbk COLLATE gbk_chinese_ci DEFAULT NULL,
                           `observationType` enum('集中隔离','居家隔离') CHARACTER SET gbk COLLATE gbk_chinese_ci DEFAULT NULL,
                           `observationDate` date DEFAULT NULL,
                           `whereabouts` enum('有湖北，温州等疫情发生地旅游史或居住史',
                               '有接触过湖北、温州等疫情发生地的人员',
                               '有接触过疑似或确诊为新型冠状病毒感染者',
                               '身边有多人出现发热、乏力、咳嗽、咽痛等',
                               '无相关接触史') CHARACTER SET gbk COLLATE gbk_chinese_ci DEFAULT NULL,
                           `declareDate` date NOT NULL,
                           PRIMARY KEY (`did`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=gbk ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of declare
-- ----------------------------
INSERT INTO `declare` VALUES ('1', '1', '体温37.3度以下', '否', null, null, null, null, null, null, null, '有湖北，温州等疫情发生地旅游史或居住史', '2020-06-19');
INSERT INTO `declare` VALUES ('2', '1', '体温37.3度以下', '否', null, null, null, null, null, null, null, '有接触过湖北、温州等疫情发生地的人员', '2020-06-20');
INSERT INTO `declare` VALUES ('3', '2', '体温37.3度以下', '是', '是', '是', '是', '是', '否', null, null, '有接触过疑似或确诊为新型冠状病毒感染者', '2020-06-19');
INSERT INTO `declare` VALUES ('4', '3', '体温37.3度以上', '是', '是', '是', '是', '是','是', '集中隔离', '2020-06-18', '身边有多人出现发热、乏力、咳嗽、咽痛等', '2020-06-18');
INSERT INTO `declare` VALUES ('5', '4', '体温37.3度以上', '否', null, null, null, null, null, null, null,'无相关接触史', '2020-06-20');
