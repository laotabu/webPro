package com.dgut.bean;

public class Admin {
    //                             `aid` int NOT NULL AUTO_INCREMENT,
//                         `adminName` varchar(20) NOT NULL,
//                         `password` varchar(20) NOT NULL,
//                         `adminImgae` varchar(20) DEFAULT NULL,
    private Integer aid; //id号
    private String adminName;//用户名
    private String password;//密码
    private String adminImage;//管理员头像文件名

    public Integer getAid() {
        return aid;
    }

    public void setAid(Integer aid) {
        this.aid = aid;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAdminImage() {
        return adminImage;
    }

    public void setAdminImage(String adminImage) {
        this.adminImage = adminImage;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "aid=" + aid +
                ", adminName='" + adminName + '\'' +
                ", password='" + password + '\'' +
                ", adminImage='" + adminImage + '\'' +
                '}';
    }
}
