package com.dgut.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class Declare {
    private Integer did;
    private Integer uid;
    private String temperature;      //体温
    private String isDiscomfort;    //是否有不适情况
//    private List discomfort;        //不适情况
    private String cough;              //咳嗽
    private String weak;               //乏力
    private String dizzy;               //头晕
    private String diarrhea;            //腹泻
    private String isObservation;   //是否接收医学观察
    private String observationType; //医学观察方式
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date observationDate;   //隔离观察日期
    private String whereabouts;     //接触史
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date declareDate;       //申报日期

    public Integer getDid() {
        return did;
    }

    public void setDid(Integer did) {
        this.did = did;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getIsDiscomfort() {
        return isDiscomfort;
    }

    public void setIsDiscomfort(String isDiscomfort) {
        this.isDiscomfort = isDiscomfort;
    }

    public String getCough() {
        return cough;
    }

    public void setCough(String cough) {
        this.cough = cough;
    }

    public String getWeak() {
        return weak;
    }

    public void setWeak(String weak) {
        this.weak = weak;
    }

    public String getDizzy() {
        return dizzy;
    }

    public void setDizzy(String dizzy) {
        this.dizzy = dizzy;
    }

    public String getDiarrhea() {
        return diarrhea;
    }

    public void setDiarrhea(String diarrhea) {
        this.diarrhea = diarrhea;
    }

    public String getIsObservation() {
        return isObservation;
    }

    public void setIsObservation(String isObservation) {
        this.isObservation = isObservation;
    }

    public String getObservationType() {
        return observationType;
    }

    public void setObservationType(String observationType) {
        this.observationType = observationType;
    }

    public Date getObservationDate() {
        return observationDate;
    }

    public void setObservationDate(Date observationDate) {
        this.observationDate = observationDate;
    }

    public String getWhereabouts() {
        return whereabouts;
    }

    public void setWhereabouts(String whereabouts) {
        this.whereabouts = whereabouts;
    }

    public Date getDeclareDate() {
        return declareDate;
    }

    public void setDeclareDate(Date declareDate) {
        this.declareDate = declareDate;
    }

    @Override
    public String toString() {
        return "Declare{" +
                "did=" + did +
                ", uid=" + uid +
                ", temperature='" + temperature + '\'' +
                ", isDiscomfort='" + isDiscomfort + '\'' +
                ", cough='" + cough + '\'' +
                ", weak='" + weak + '\'' +
                ", dizzy='" + dizzy + '\'' +
                ", diarrhea='" + diarrhea + '\'' +
                ", isObservation='" + isObservation + '\'' +
                ", observationType='" + observationType + '\'' +
                ", observationDate=" + observationDate +
                ", whereabouts='" + whereabouts + '\'' +
                ", declareDate=" + declareDate +
                '}';
    }
}
