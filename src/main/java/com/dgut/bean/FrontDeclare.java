package com.dgut.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;
//用于接收前端的疫情表

//`did` int NOT NULL AUTO_INCREMENT,
//        `uid` int NOT NULL,
//        `temperature` enum('体温37.3度以下','体温37.3度以上') CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL,
//        `isDiscomfort` enum('是','否') CHARACTER SET gbk COLLATE gbk_chinese_ci DEFAULT NULL,
//        `discomfort` varchar(30) CHARACTER SET gbk COLLATE gbk_chinese_ci DEFAULT NULL,
//        `isObservation` enum('是','否') CHARACTER SET gbk COLLATE gbk_chinese_ci DEFAULT NULL,
//        `observationType` enum('集中隔离','居家隔离') CHARACTER SET gbk COLLATE gbk_chinese_ci DEFAULT NULL,
//        `observationDate` date DEFAULT NULL,
//        `Whereabouts` varchar(50) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL,
//        `declareDate` date NOT NULL,
public class FrontDeclare {
    private Integer did;
    private Integer uid;
    private String temperature;      //体温
    private String isDiscomfort;    //是否有不适情况
    private List discomfort;        //不适情况
    private String isObservation;   //是否接收医学观察
    private String observationType; //医学观察方式
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date observationDate;   //隔离观察日期
    private String whereabouts;     //接触史
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date declareDate;       //申报日期

    public Integer getDid() {
        return did;
    }

    public void setDid(Integer did) {
        this.did = did;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getIsDiscomfort() {
        return isDiscomfort;
    }

    public void setIsDiscomfort(String isDiscomfort) {
        this.isDiscomfort = isDiscomfort;
    }

    public List getDiscomfort() {
        return discomfort;
    }

    public void setDiscomfort(List discomfort) {
        this.discomfort = discomfort;
    }

    public String getIsObservation() {
        return isObservation;
    }

    public void setIsObservation(String isObservation) {
        this.isObservation = isObservation;
    }

    public String getObservationType() {
        return observationType;
    }

    public void setObservationType(String observationType) {
        this.observationType = observationType;
    }

    public Date getObservationDate() {
        return observationDate;
    }

    public void setObservationDate(Date observationDate) {
        this.observationDate = observationDate;
    }

    public String getWhereabouts() {
        return whereabouts;
    }

    public void setWhereabouts(String whereabouts) {
        this.whereabouts = whereabouts;
    }

    public Date getDeclareDate() {
        return declareDate;
    }

    public void setDeclareDate(Date declareDate) {
        this.declareDate = declareDate;
    }

    @Override
    public String toString() {
        return "FrontDeclare{" +
                "did=" + did +
                ", uid=" + uid +
                ", temperature='" + temperature + '\'' +
                ", isDiscomfort='" + isDiscomfort + '\'' +
                ", discomfort=" + discomfort +
                ", isObservation='" + isObservation + '\'' +
                ", observationType='" + observationType + '\'' +
                ", observationDate=" + observationDate +
                ", whereabouts='" + whereabouts + '\'' +
                ", declareDate=" + declareDate +
                '}';
    }
}
