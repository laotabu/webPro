package com.dgut.service.impl;

import com.dgut.bean.Admin;
import com.dgut.mapper.AdminMapper;
import com.dgut.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminMapper adminMapper;
    @Override
    public Admin findAdminById(Integer aid) {
        return adminMapper.findAdminById(aid);
    }

    @Override
    public Admin findAdminByName(String adminName) {
        return adminMapper.findAdminByName(adminName);
    }
}
