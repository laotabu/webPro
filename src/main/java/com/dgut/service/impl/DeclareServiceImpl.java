package com.dgut.service.impl;

import com.dgut.bean.Declare;
import com.dgut.bean.DeclareAndUser;
import com.dgut.bean.FrontDeclare;
import com.dgut.mapper.DeclareMapper;
import com.dgut.service.DeclareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DeclareServiceImpl implements DeclareService {
    @Autowired
    private DeclareMapper declareMapper;

    @Override
    public List<Declare> findAll() {
        return declareMapper.findAll();
    }

    @Override
    public void addDeclare(Declare declare) {
        declareMapper.addDeclare(declare);
    }

    @Override
    public List<Declare> findByUid(Integer uid) {
        return declareMapper.findByUid(uid);
    }

    @Override
    public List<Declare> searchPerson(Integer uid, String keyword) {
        return declareMapper.searchPerson(uid,keyword);
    }

    @Override
    public Declare FrontD_TO_D(FrontDeclare frontDeclare) {
        Declare declare = new Declare();
        declare.setDid(frontDeclare.getDid());
        declare.setUid(frontDeclare.getUid());
        declare.setTemperature(frontDeclare.getTemperature());
        declare.setIsDiscomfort(frontDeclare.getIsDiscomfort());
        declare.setIsObservation(frontDeclare.getIsObservation());
        declare.setObservationType(frontDeclare.getObservationType());
        declare.setObservationDate(frontDeclare.getObservationDate());
        declare.setWhereabouts(frontDeclare.getWhereabouts());
        declare.setDeclareDate(frontDeclare.getDeclareDate());
        if(declare.getIsDiscomfort().equals("是")){
            for(Object item : frontDeclare.getDiscomfort()){
                if(item.equals("咳嗽"))
                    declare.setCough("是");
                else if(item.equals("乏力"))
                    declare.setWeak("是");
                else if(item.equals("头晕"))
                    declare.setDizzy("是");
                else if(item.equals("腹泻"))
                    declare.setDiarrhea("是");
            }
        }
        return declare;
    }

    @Override
    public FrontDeclare D_TO_FrontD(Declare declare) {
        FrontDeclare frontDeclare = new FrontDeclare();
        List list = new ArrayList();
        frontDeclare.setDid(declare.getDid());
        frontDeclare.setUid(declare.getUid());
        frontDeclare.setTemperature(declare.getTemperature());
        frontDeclare.setIsDiscomfort(declare.getIsDiscomfort());
        frontDeclare.setIsObservation(declare.getIsObservation());
        frontDeclare.setObservationType(declare.getObservationType());
        frontDeclare.setObservationDate(declare.getObservationDate());
        frontDeclare.setWhereabouts(declare.getWhereabouts());
        frontDeclare.setDeclareDate(declare.getDeclareDate());
        if(frontDeclare.getIsDiscomfort().equals("是")){

            if(declare.getCough() != null && declare.getCough().equals("是"))
                list.add("咳嗽 ");
            if(declare.getWeak() != null && declare.getWeak().equals("是"))
                list.add("乏力 ");
            if(declare.getDizzy() != null && declare.getDizzy().equals("是") )
                list.add("头晕 ");
            if(declare.getDiarrhea() != null && declare.getDiarrhea().equals("是"))
                list.add("腹泻 ");
        }
        frontDeclare.setDiscomfort(list);
        return frontDeclare;
    }

//    转化格式
    public DeclareAndUser setValueDiscomfort(DeclareAndUser declareAndUser){
        List list = new ArrayList();
        if(declareAndUser.getIsDiscomfort().equals("是")){
            if(declareAndUser.getCough()!=null && declareAndUser.getCough().equals("是"))
                list.add("咳嗽 ");
            if(declareAndUser.getWeak() != null && declareAndUser.getWeak().equals("是"))
                list.add("乏力 ");
            if(declareAndUser.getDizzy() != null && declareAndUser.getDizzy().equals("是") )
                list.add("头晕 ");
            if(declareAndUser.getDiarrhea() != null && declareAndUser.getDiarrhea().equals("是"))
                list.add("腹泻 ");
        }
        declareAndUser.setDiscomfort(list);
        return declareAndUser;
    }

    @Override
    public List<DeclareAndUser> findAllByAdmin() {
        return declareMapper.findAllByAdmin();
    }

    @Override
    public List<DeclareAndUser> searchDeclare(String keyword) {
        return declareMapper.searchDeclare(keyword);
    }
}
