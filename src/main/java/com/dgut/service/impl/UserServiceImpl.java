package com.dgut.service.impl;


import com.dgut.bean.User;
import com.dgut.mapper.UserMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dgut.service.UserService;

import java.util.List;


@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public List<User> findAll() {
        return userMapper.findAll();
    }

    @Override
    public User findUserById(Integer uid) {
        return userMapper.findUserById(uid);
    }

    @Override
    public User findUserByUsername(String username) {
        return userMapper.findUserByUsername(username);
    }

    @Override
    public void addUser(String username, String password) {
        userMapper.addUser(username,password);
    }

    @Override
    public void addUserByAdmin(User user) {
        userMapper.addUserByAdmin(user);
    }

    @Override
    public void updateUserInformation(User user) {
        userMapper.updateUserInformation(user);
    }

    @Override
    public void updateUserByAdmin(User user) {
        userMapper.updateUserByAdmin(user);
    }

    @Override
    public void updateUserImage(Integer uid, String userImage) {
        userMapper.updateUserImage(uid,userImage);
    }

    @Override
    public List<User> searchUser(String keyword) {
        return userMapper.searchUser(keyword);
    }

    @Override
    public void deleteUserByAdmin(Integer uid) {
        userMapper.deleteUserByAdmin(uid);
    }


}
