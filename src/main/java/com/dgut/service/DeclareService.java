package com.dgut.service;

import com.dgut.bean.Declare;
import com.dgut.bean.DeclareAndUser;
import com.dgut.bean.FrontDeclare;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DeclareService {
    //查找所以疫情申报的情况
    public List<Declare> findAll();

    //添加疫情申报的情况
    public void addDeclare(Declare declare);

    //根据uid查找疫情的记录
    public List<Declare> findByUid(Integer uid);

    //模糊查询
    public List<Declare> searchPerson(@Param("uid") Integer uid, @Param("keyword")String keyword);

    //    前端FrontDeclare转化成后端Declare
    public Declare FrontD_TO_D(FrontDeclare frontDeclare);

    //    后端Declare转化成前端FrontDeclare
    public FrontDeclare D_TO_FrontD(Declare declare);

    //    转化格式
    public DeclareAndUser setValueDiscomfort(DeclareAndUser declareAndUser);

    //管理员查看所有的疫情登记
    public List<DeclareAndUser> findAllByAdmin();

    //管理员模糊查询疫情表
    public List<DeclareAndUser> searchDeclare(@Param("keyword")String keyword);
}
