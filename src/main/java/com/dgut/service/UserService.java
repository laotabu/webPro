package com.dgut.service;


import com.dgut.bean.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserService {
    //    查找所有用户
    public List<User> findAll();

    //    根据id查找用户
    public User findUserById(Integer uid);

    //    根据用户名查找用户
    public User findUserByUsername(String username);

    //    注册时添加用户
    public void addUser(String username, String password);

    //管理员添加用户
    public void addUserByAdmin(User user);

    //    添加或修改用户信息
    public void updateUserInformation(User user);

    //    管理修改用户
    public void updateUserByAdmin(User user);

    //    修改图像名称
    public void updateUserImage(Integer uid, String userImage);

    //管理员模糊查询用户
    public List<User> searchUser(@Param("keyword") String keyword);

    //管理员根据Id删除用户
    public void deleteUserByAdmin(Integer uid);
}
