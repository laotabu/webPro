package com.dgut.mapper;

import com.dgut.bean.Admin;

public interface AdminMapper {
    /**
     * 根据id找管理员
     * @param aid
     * @return
     */
    public Admin findAdminById(Integer aid);
    /**
     * 根据用户名找管理员
     * @param adminName
     * @return
     */
    public Admin findAdminByName(String adminName);

}
