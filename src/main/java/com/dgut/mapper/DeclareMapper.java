package com.dgut.mapper;

import com.dgut.bean.Declare;
import com.dgut.bean.DeclareAndUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DeclareMapper {
    //查找所有疫情申报的情况
    public List<Declare> findAll();

    //添加疫情申报的情况
    public void addDeclare(Declare declare);

    //根据uid查找疫情的记录
    public List<Declare> findByUid(Integer uid);

    //模糊查询,查找申报记录
    public List<Declare> searchPerson(@Param("uid") Integer uid,@Param("keyword")String keyword);

    //管理员查找用户和疫情表的所有信息
    public List<DeclareAndUser> findAllByAdmin();

    //管理员模糊查询疫情表
    public List<DeclareAndUser> searchDeclare(@Param("keyword")String keyword);

}
