package com.dgut.mapper;


import com.dgut.bean.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface UserMapper {
    //    查找所有用户
//    @Select("select * from user")
    public List<User> findAll();

    //    根据id查找用户
//    @Select("select * from user where uid = #{uid}")
    public User findUserById(Integer uid);

    //    根据用户名查找用户
//    @Select("select * from user where username = #{username}")
    public User findUserByUsername(String username);

    //    注册时添加用户
//    @Insert("INSERT INTO `user` (username,password)VALUES(#{username},#{password})")
    public void addUser(@Param("username") String username, @Param("password") String password);

    //管理员添加用户
    public void addUserByAdmin(User user);

    //    添加或修改用户信息
//    @Update("        update `user` set idNumber=#{idNumber},\n" +
//            "        realName=#{realName},\n" +
//            "        sex=#{sex},\n" +
//            "        birthday=#{birthday},\n" +
//            "        phone=#{phone},\n" +
//            "        address=#{address} where uid=#{uid}")
    public void updateUserInformation(User user);

//    管理修改用户
    public void updateUserByAdmin(User user);

    //    修改图像名称
//    @Update("update `user` set userImage=#{userImage} where uid = #{uid}")
    public void updateUserImage(@Param("uid") Integer uid, @Param("userImage") String userImage);

    //管理员模糊查询用户
    public List<User> searchUser(@Param("keyword") String keyword);

    //管理员根据Id删除用户
    public void deleteUserByAdmin(Integer uid);
}
