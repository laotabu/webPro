package com.dgut.controller;

import com.dgut.bean.Admin;
import com.dgut.bean.DeclareAndUser;
import com.dgut.bean.User;
import com.dgut.service.AdminService;
import com.dgut.service.DeclareService;
import com.dgut.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private AdminService adminService;
    @Autowired
    private UserService userService;
    @Autowired
    private DeclareService declareService;

    @RequestMapping("/")
    public String main() {
        return "adminMain";
    }

    @PostMapping("/login")
    public ModelAndView login(String name, String password, HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        Admin admin = adminService.findAdminByName(name);
//        System.out.println(admin);
        //登录成功进入主页，登录失败重新进入登录页
        //判断用户名是否存在
        if (admin == null) {
            modelAndView.addObject("msg1", "用户名不存在");
            modelAndView.setViewName("login");
        } else {
            //判断密码是否正确
            if (password.equals(admin.getPassword())) {
                //登录成功，user加入session
                request.getSession().setAttribute("admin", admin);
                modelAndView.addObject("admin", admin);
                modelAndView.setViewName("adminMain");
            } else {
                //登录失败返回登录页面
                modelAndView.addObject("msg1", "密码错误");
                modelAndView.setViewName("login");
            }
        }


        return modelAndView;
    }


    //管理员的个人信息
    @RequestMapping("/adminInformation")
    public @ResponseBody
    Admin adminInformation(HttpSession session) {
        Admin sessionAdmin = (Admin) session.getAttribute("admin");
        Admin admin = adminService.findAdminById(sessionAdmin.getAid());
        return admin;
    }

    //所有用户的页面
    @RequestMapping("/allUserTable")
    public String allUserTable() {
        return "allUserTable";
    }

    //所有疫情表的页面
    @RequestMapping("/allDeclareTable")
    public String allDeclareForm(){return "allDeclareTable";}

    //所有用户的数据
    @RequestMapping("/allUserTableJson")
    public @ResponseBody
    List<User> allUserTableJson() {
        List<User> users = userService.findAll();
        return users;
    }

    //模糊查询所有用户的数据
    @RequestMapping("/searchUserJson")
    public @ResponseBody
    List<User> searchUserJson(String keyword){
        List<User> users = userService.searchUser(keyword);
        return users;
    }

    //根据id返回json数据
    @RequestMapping("/findUserByUidJson")
    public @ResponseBody
    User findUserByUid(Integer uid) {
        return userService.findUserById(uid);
    }


    //    管理修改的用户信息发送的json
    @PostMapping("/updateUserByUidJson")
    public void updateUserByUidJson(@RequestBody User actionUser){
//        System.out.println(actionUser);
//        数据库更新数据
        userService.updateUserByAdmin(actionUser);
    }

    //管理员添加用户
    @PostMapping("/addUserByAdminJson")
    public void addUserByAdminJson(@RequestBody User actionUser){
//        System.out.println(actionUser);
        userService.addUserByAdmin(actionUser);
    }

    //管理员删除用户
    @RequestMapping("/deleteUserByAdmin")
    public void deleteUserByAdmin(Integer uid){
        System.out.println(uid);
        userService.deleteUserByAdmin(uid);
    }

    //管理员查看所有的疫情登记
    @RequestMapping("/allDeclareAndUserJson")
    public @ResponseBody List<DeclareAndUser> allDeclareAndUserJson(){
        List<DeclareAndUser> declareAndUsers = declareService.findAllByAdmin();
        for(DeclareAndUser declareAndUser : declareAndUsers){
            declareAndUser = declareService.setValueDiscomfort(declareAndUser);
        }
        return declareAndUsers;
    }
    //管理员模糊查询的疫情登记
    @RequestMapping("/searchDeclareJson")
    public @ResponseBody List<DeclareAndUser> searchDeclareJson(String keyword){
        List<DeclareAndUser> declareAndUsers = declareService.searchDeclare(keyword);
        for(DeclareAndUser declareAndUser : declareAndUsers){
            declareAndUser = declareService.setValueDiscomfort(declareAndUser);
        }
        return declareAndUsers;
    }

    //    注销admin
    @RequestMapping("/logoutAdmin")
    public String logoutAdmin(HttpSession session) {
        session.removeAttribute("admin");
        return "login";
    }
}
