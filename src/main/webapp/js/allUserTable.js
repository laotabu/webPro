var persontable = new Vue({
    el: '#showData',
    data: function () {
        return {
            keyword: '',
            tableData: [],
            FormVisible1: false,
            FormVisible2: false,
            FormVisible3: false,
            deleteUid: '',
            updateForm: {
                username: '',
                password: '',
                idNumber: '',
                realName: '',
                sex: '',
                birthday: '',
                phone: "",
                address: '',
                userImage: "",
            },
            addForm: {
                username: '',
                password: '',
                idNumber: '',
                realName: '',
                sex: '',
                birthday: '',
                phone: "",
                address: '',
                userImage: "",
            },
            formLabelWidth: '120px',
        }
    },
    mounted: function () {
        var that = this;
        //钩子函数，初始化信息
        axios.get("/admin/allUserTableJson")
            .then(function (response) {
                // console.log(response.data);
                that.tableData = response.data;
            }, function (err) {
            });
    },

    methods: {
        searchPerson() {
            var that = this;
            axios.get("/admin/searchUserJson?keyword=" + this.keyword)
                .then(function (response) {
                    that.tableData = response.data;
                })
        },

        // 修改用户
        updateUser(uid) {
            this.FormVisible1 = true;
            var that = this;
            axios.get("/admin/findUserByUidJson?uid=" + uid)
                .then(function (response) {
                    that.updateForm = response.data;
                });
        },
        updateUserDeal() {
            this.FormVisible1 = false;
            let config = {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            };
            //发送信息
            axios.post('/admin/updateUserByUidJson', this.updateForm, this.config)
                .then(function (response) {
                });

            // 模拟延时加载
            var start = Number(new Date());
            while (start + 2000 > Number(new Date())) {
                console.log(start);
                var that = this;
                //初始化信息
                axios.get("/admin/allUserTableJson")
                    .then(function (response) {
                        that.tableData = response.data;
                        console.log(response.data);
                    }, function (err) {
                    });
                break;
            }


        },

        // 添加用户
        addUser() {
            this.FormVisible2 = true;
        },
        addUserDeal() {
            this.FormVisible2 = false;
            // console.log(this.addForm);
            let config = {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            };
            axios.post('/admin/addUserByAdminJson', this.addForm, this.config)
                .then(function (response) {
                });
            // 模拟延时加载
            var start = Number(new Date());
            while (start + 2000 > Number(new Date())) {
                console.log(start);
                var that = this;
                //初始化信息
                axios.get("/admin/allUserTableJson")
                    .then(function (response) {
                        that.tableData = response.data;
                        console.log(response.data);
                    }, function (err) {
                    });
                break;
            }

        },

        // 删除用户
        deleteUser(uid) {
            this.FormVisible3 = true;
            this.deleteUid = uid;
            // console.log(this.deleteUid)
        },
        deleteUserDeal() {

            axios.get("/admin/deleteUserByAdmin?uid=" + this.deleteUid)
                .then(function (response) {
                });

            // 模拟延时加载
            var start = Number(new Date());
            while (start + 2000 > Number(new Date())) {
                console.log(start);
                var that = this;
                //初始化信息
                axios.get("/admin/allUserTableJson")
                    .then(function (response) {
                        that.tableData = response.data;
                        console.log(response.data);
                    }, function (err) {
                    });
                break;
            }
            this.FormVisible3 = false;
        },
    }
})