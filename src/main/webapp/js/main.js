var vue1 = new Vue({
    el: '#main',
    data: function () {
        return {
            circleUrl: "",//头像属性
            username: "",//用户名称
            isShow: false,
        }
    },
    mounted: function () {
        var that = this;
        //钩子函数，初始化信息
        axios.get("/user/personalInformationJson")
            .then(function (response) {
                that.circleUrl = response.data.userImage;
                that.username = response.data.username;
            }, function (err) {
            });
    },
    methods: {}
})