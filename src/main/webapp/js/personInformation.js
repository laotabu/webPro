var app = new Vue({
    el: "#personInformation",
    data() {
        return {
            form: {
                idNumber: '',
                realName: '',
                sex: '',
                birthday: '',
                phone: "",
                address: '',
                userImage: "",
            },
        }
    },
    mounted: function () {
        var that = this;
        //钩子函数，初始化信息
        axios.get("/user/personalInformationJson")
            .then(function (response) {
                that.form = response.data;
            }, function (err) { });
    },
    methods: {
        onSubmit() {
            // 身份证验证正则表达式
            var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
            console.log("success");
           if(reg.test(this.form.idNumber)) {
                    // 姓名简单验证
                   var reg = /^[\u4E00-\u9FA5]{2,6}$/;
                   if (reg.test(this.form.realName)) {
                       // 验证电话号码
                       var reg = /^[1-9]\d{3,11}$/
                       if(reg.test(this.form.phone)){
                           let config = {
                               headers: {
                                   'Content-Type': 'multipart/form-data'
                               }
                           };
                           //发送信息
                           axios.post('/user/updatePersonalInformation', this.form, this.config)
                               .then(function (response) {
                                   alert("修改成功")
                               });
                       }else {
                           alert("电话格式出错，仅支持3-11位数字");
                       }
                   } else {
                       alert("姓名出错，请确认是否为2-6个汉字组成")
                       return null
                    }
           }else{
               alert("身份证格式有误");
               return null;
           }

        },
        handleAvatarSuccess(res, file) {
            // this.form.userImage = URL.createObjectURL(file.raw);
            // console.log("success");
            location.reload(true);
        },
        beforeAvatarUpload(file) {
            const isJPG = file.type === 'image/jpeg';
            const isPNG = file.type === 'image/png';
            const isLt2M = file.size < 5 * 1024 * 1024;

            if (!isJPG && !isPNG) {
                this.$message.error('上传头像图片只能是 JPG 和png格式!');
            }
            if (!isLt2M) {
                this.$message.error('上传头像图片大小不能超过 5MB!');
            }
            return isJPG && isLt2M;
        }
    }
})