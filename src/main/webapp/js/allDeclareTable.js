var persontable = new Vue({
    el: '#showData',
    data: function () {
        return {
            keyword:'',
            tableData: []
        }
    },
    mounted: function () {
        var that = this;
        //钩子函数，初始化信息
        axios.get("/admin/allDeclareAndUserJson")
            .then(function (response) {
                // console.log(response.data);
                that.tableData = response.data;
            }, function (err) {
            });
    },
    methods: {
        searchDeclare(){
            var that = this;
            axios.get("/admin/searchDeclareJson?keyword="+this.keyword)
                .then(function (response) {
                    that.tableData = response.data;
                    // console.log(that.tableData);
                })
        }
    }
})