var persontable = new Vue({
    el: '#showData',
    data: function () {
        return {
            keyword:'',
            tableData: []
        }
    },
    mounted: function () {
        var that = this;
        //钩子函数，初始化信息
        axios.get("/declare/personRecord")
            .then(function (response) {
                // console.log(response.data);
                that.tableData = response.data;
            }, function (err) {
            });
    },
    methods: {
        searchPerson(){
            var that = this;
            axios.get("/declare/searchPersonJson?keyword="+this.keyword)
                .then(function (response) {
                    that.tableData = response.data;
                    console.log(that.tableData);
                })
        }
    }
})