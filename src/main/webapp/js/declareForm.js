var vue2 = new Vue({
    el: "#declareForm",
    data() {
        return {
            declareForm: {
                temperature: '',
                isDiscomfort: '是',
                discomfort: [],
                isObservation: '是',
                observationType: '',
                observationDate: '',
                whereabouts: '',
            },
            // rules: {
            //     temperature: [
            //         { required: true, message: '请填写体温', trigger: 'blur' },
            //         // { type: 'number', message: '体温必须为数字值'},
            //         {pattern:/^(((3[5-9]|4[0-3])(\.\d)?))$/,message:'请正确填写体温',trigger:'blur'}
            //     ],
            //     isDiscomfort: [
            //         { required: true, message: '请选择是否有不适', trigger: 'change' }
            //     ],
            //     discomfort: [
            //         { required: true,message:'请选择身体不适情况'}
            //     ],
            //     isObservation: [
            //         {required: true, message: '请选择是否隔离中', trigger: 'change'}
            //     ],
            //     observationType: [
            //         {required:true,message:'请选择隔离方式', trigger: 'change'}
            //     ],
            //     startDate: [
            //         {required: true, message: '请选择日期', trigger: 'change' }
            //     ],
            //     endDate: [
            //         {required: true, message: '请选择日期', trigger: 'change' }
            //     ],
            //     Whereabouts: [
            //         {required:true, message: '请选择接触史', trigger: 'change'}
            //     ]
            // }
        };
    },
    methods: {
        submitForm() {
            let config = {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            };
            //发送信息
            axios.post('/declare/addDeclare', this.declareForm, this.config)
                .then(function (response) {

                }.catch(function (response) {

                }))

            // this.$refs[formName].validate((valid) => {
            //     if (valid) {
            //         // alert('submit!');
            //         // alert(JSON.stringify(this.declareForm))
            //         axios.post('/user/testDeclare',JSON.stringify(this.declareForm)).then((response) => {
            //             console.log(response)
            //         })
            //     } else {
            //         console.log('error submit!!');
            //         return false;
            //     }

            // });

        },
        resetForm(formName) {
            this.$refs[formName].resetFields();
        }
    }
})