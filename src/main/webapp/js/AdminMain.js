var vue5 = new Vue({
    el: '#main',
    data: function () {
        return {
            circleUrl: "",//头像属性
            adminName: "",//用户名称
            isShow: false,
        }
    },
    mounted: function () {
        var that = this;
        //钩子函数，初始化信息
        axios.get("/admin/adminInformation")
            .then(function (response) {
                that.circleUrl = response.data.adminImage;
                that.adminName = response.data.adminName;
            }, function (err) {
            });
    },
    methods: {}
})