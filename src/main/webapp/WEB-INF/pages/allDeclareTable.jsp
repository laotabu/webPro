<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <title>showData</title>
    <meta charset="UTF-8">
    <!-- import CSS -->
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
    <link rel="stylesheet" href="/css/allDeclareTable.css">
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</head>

<body>
<div id="showData">
    <el-input v-model="keyword" @keyup.enter="searchDeclare" placeholder="请输入内容" style="width:300px" clearable>
        <el-button type="primary" @click="searchDeclare" slot="append" icon="el-icon-search">搜索</el-button>
    </el-input>
    <template>
        <el-table :data="tableData" height="450" border style="width: 100%" header-row-class-name="center">
            <el-table-column prop="declareDate" label="申报日期" width="120">
            </el-table-column>
            <el-table-column prop="user.username" label="用户名" width="120">
            </el-table-column>
            <el-table-column prop="user.realName" label="真实姓名" width="120">
            </el-table-column>
            <el-table-column  prop="temperature" label="体温" width="150">
            </el-table-column>
            <el-table-column prop="isDiscomfort" label="是否有不适情况" width="150">
            </el-table-column>
            <el-table-column prop="discomfort" label="不适情况" width="120">
            </el-table-column>
            <el-table-column prop="isObservation" label="是否接受医学观察" width="100">
            </el-table-column>
            <el-table-column prop="observationType" label="医学观察方式" width="120">
            </el-table-column>
            <el-table-column prop="observationDate" label="隔离观察日期" width="120">
            </el-table-column>
            <el-table-column prop="whereabouts" label="接触史" width="332">
            </el-table-column>

            <%--<el-table-column
                    fixed="right"
                    label="操作"
                    width="100">
                <template slot-scope="scope">
                    <el-button @click="handleClick(scope.row)" type="text" size="small">详情</el-button>
                    <el-button type="text" size="small">编辑</el-button>
                    <a :href="getDetail(scope.row.city)" target="main">haha</a>
                </template>
            </el-table-column>--%>
        </el-table>

    </template>
</div>
</body>
<!-- import Vue before Element -->
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<!-- import JavaScript -->
<script src="https://unpkg.com/element-ui/lib/index.js"></script>
<script src="/js/allDeclareTable.js"></script>
</html>