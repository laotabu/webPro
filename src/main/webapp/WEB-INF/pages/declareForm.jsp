<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/6/24
  Time: 22:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="UTF-8">
    <!-- import CSS -->
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
    <!-- <link rel="stylesheet" href="./css/elementUi.css"> -->
    <!-- <link rel="stylesheet" href="./css/main.css"> -->
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <!-- import Vue before Element -->
    <script src="https://unpkg.com/vue/dist/vue.js"></script>
    <!-- <script src="./js/vue.js"></script> -->
    <script src="https://unpkg.com/element-ui/lib/index.js"></script>
    <!-- <script src="./js/index.js"></script> -->
    <link rel="stylesheet" href="/css/declareForm.css">
</head>

<body>
<div id="declareForm">


    <!-- <el-form :model="declareForm" :rules="rules" ref="declareForm" label-position="top"> -->
    <el-form :model="declareForm" ref="declareForm">

        <el-form-item label="体温" prop="temperature">
            <!-- <el-input v-model="declareForm.temperature" style="width: 150px"></el-input> -->
            <el-select v-model="declareForm.temperature" placeholder="请选择温度">
                <el-option label="体温37.3度以下" value="体温37.3度以下"></el-option>
                <el-option label="体温37.3度以上" value="体温37.3度以上"></el-option>
            </el-select>
        </el-form-item>

        <el-form-item label="是否有不适" prop="isDiscomfort">
            <el-radio v-model="declareForm.isDiscomfort" label="是"></el-radio>
            <el-radio v-model="declareForm.isDiscomfort" label="否"></el-radio>
        </el-form-item>

        <el-form-item label="身体不适情况" prop="discomfort" v-if="declareForm.isDiscomfort=='是'">
            <el-checkbox-group v-model="declareForm.discomfort">
                <el-checkbox label="咳嗽" value="discomfort"></el-checkbox>
                <el-checkbox label="乏力" value="discomfort"></el-checkbox>
                <el-checkbox label="头晕" value="discomfort"></el-checkbox>
                <el-checkbox label="腹泻" value="discomfort"></el-checkbox>
            </el-checkbox-group>
        </el-form-item>

        <el-form-item label="是否正接受医学观察" prop="isObservation">
            <el-radio v-model="declareForm.isObservation" label="是"></el-radio>
            <el-radio v-model="declareForm.isObservation" label="否"></el-radio>
        </el-form-item>

        <el-form-item label="医学观察方式" prop="observationType" v-if="declareForm.isObservation=='是'">
            <el-select v-model="declareForm.observationType" placeholder="请选择观察方式">
                <el-option label="集中隔离" value="集中隔离"></el-option>
                <el-option label="居家隔离" value="居家隔离"></el-option>
            </el-select>
        </el-form-item>

        <el-form-item label="隔离观察日期" required v-if="declareForm.isObservation=='是'">
            <el-form-item prop="observationDate">
                <el-date-picker type="date" value-format="yyyy-MM-dd" placeholder="选择起始日期"
                                v-model="declareForm.observationDate"></el-date-picker>
            </el-form-item>
        </el-form-item>

        <el-form-item label="接触史" prop="whereabouts">
            <el-select v-model="declareForm.whereabouts" placeholder="请选择接触史">
                <el-option label="有湖北，温州等疫情发生地旅游史或居住史" value="有湖北，温州等疫情发生地旅游史或居住史"></el-option>
                <el-option label="有接触过湖北、温州等疫情发生地的人员" value="有接触过湖北、温州等疫情发生地的人员"></el-option>
                <el-option label="有接触过疑似或确诊为新型冠状病毒感染者" value="有接触过疑似或确诊为新型冠状病毒感染者"></el-option>
                <el-option label="身边有多人出现发热、乏力、咳嗽、咽痛等" value="身边有多人出现发热、乏力、咳嗽、咽痛等"></el-option>
                <el-option label="无相关接触史" value="无相关接触史"></el-option>
            </el-select>
        </el-form-item>

        <el-form-item>
            <a href="/declare/personTable" target="main">
                <el-button type="primary" @click="submitForm()">提交</el-button>
            </a>
            <el-button @click="resetForm('declareForm')">重置</el-button>
        </el-form-item>
    </el-form>
</div>
</body>
<script src="/js/declareForm.js"></script>

