<%--
  Created by IntelliJ IDEA.
  User: 16992
  Date: 2020/6/18
  Time: 17:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>singup</title>
    <link rel="stylesheet" href="/css/login.css">
</head>
<body>
<img class="login-picture" src="/images/signup.jpg"/>
<form class="form-box back" action="/user/signup" method="post">

    <h1>Register</h1>

    <div style="color:red;text-align: center;font-size: 15px">${msg2}</div>
    <input class="input-normal" type="text" name="username" placeholder="input username">
    <input class="input-normal" type="password" name="password" placeholder="input password">
    <input class="input-normal" type="password" name="confirmPassword" placeholder="input password again">
    <button class="btn-submit" type="submit">
        Register
    </button>


    <p style="margin-top: 40px">Have a account ? You can</p>
    <p>Click here to <a href="login">Log in</a></p>

</form>
</body>
</html>

