<%--
  Created by IntelliJ IDEA.
  User: 16992
  Date: 2020/6/18
  Time: 10:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>login</title>
    <link rel="stylesheet" href="/css/login.css">
</head>
<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
<!-- import Vue before Element -->
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<!-- import JavaScript -->
<script src="https://unpkg.com/element-ui/lib/index.js"></script>
<body>
<div id="login">

    <img class="login-picture" src="/images/signup.jpg"/>
    <form class="form-box" :action="actionUrl" method="post">

        <h1>Login</h1>


        <div style="color:red;text-align: center;font-size: 15px">${msg1}</div>
        <input class="input-normal" type="text" placeholder="name" name="name" value="${user.username}">
        <input class="input-normal" type="password" placeholder="password" name="password" value="${user.password}">
        <el-switch
                style="left:25%"
                v-model="actionUrl"
                active-color="#13ce66"
                inactive-color="#AAAAAA"
                active-value="/admin/login"
                active-text="administrator"
                inactive-value="/user/login">
        </el-switch>
        <button class="btn-submit" style="margin-top: 25px" type="submit">
            LOGIN
        </button>


        <p style="margin-top: 40px">If you don't have account.Please</p>
        <p>Click here to <a href="/user/signup">Sign Up</a></p>

    </form>
</div>
<script src="/js/login.js"></script>
</body>
</html>



