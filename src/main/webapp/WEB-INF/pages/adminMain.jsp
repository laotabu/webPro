<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/6/28
  Time: 14:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="UTF-8">
    <!-- import CSS -->
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
    <link rel="stylesheet" href="/css/AdminMain.css">
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <!-- import Vue before Element -->
    <script src="https://unpkg.com/vue/dist/vue.js"></script>
    <!-- import JavaScript -->
    <script src="https://unpkg.com/element-ui/lib/index.js"></script>
</head>

<body>
<div id="main">


    <el-container>
        <el-header>
            <el-row :gutter="20">
                <el-col :span="6">
                    <div class="grid-content bg-purple"><img style="width: 400px;height: 50px;"
                                                             src="/images/title.png"></div>
                </el-col>
                <el-col :span="4" :offset="12">
                    <div class="grid-content bg-purple">
                        <div class="demo-input-suffix" style="color: #d9d9d9">
                            <%--                            <el-input @keyup.enter="" placeholder="搜索" prefix-icon="el-icon-search"--%>
                            <%--                                      v-model="inputSearch">--%>
                            <%--                            </el-input>--%>
                            欢迎你，{{adminName}}!
                        </div>
                    </div>
                </el-col>
                <el-col :span="1">
                    <div class="grid-content bg-purple">
                        <el-dropdown>
                                <span class="el-dropdown-link">
                                    <template>
                                        <el-row class="demo-avatar demo-basic">
                                            <el-col>
                                                <div class="demo-basic--circle">
                                                    <div class="block">
                                                        <el-avatar v-if="circleUrl" :size="60"
                                                                   :src="'/adminImages/'+circleUrl"></el-avatar>
                                                         <el-avatar v-else :size="60"
                                                                    src="/images/emptyHead.png"></el-avatar>
                                                    </div>
                                                </div>
                                            </el-col>
                                        </el-row>
                                    </template>
                                </span>
                            <el-dropdown-menu slot="dropdown" style="margin-top: -10px;">
                                <el-dropdown-item><a href="/admin/logoutAdmin">退出登录</a></el-dropdown-item>
                            </el-dropdown-menu>
                        </el-dropdown>
                    </div>
                </el-col>
            </el-row>
        </el-header>
        <el-container style="height: 700px;">
            <el-aside width="250px">
                <el-row class="tac">
                    <el-col>
                        <el-menu default-active="2" class="el-menu-vertical-demo" background-color="#444444"
                                 text-color="#fff" active-text-color="#EEEEEE">
                            <a href="/admin/allUserTable" style="text-decoration:none;" target="main">
                                <el-menu-item index="2" @click="isShow=true">
                                    <i class="el-icon-menu"></i>
                                    <span slot="title">用户信息</span>
                                </el-menu-item>
                            </a>
                            <a href="/admin/allDeclareTable" style="text-decoration:none;" target="main">
                                <el-menu-item index="2" @click="isShow=true">
                                    <i class="el-icon-menu"></i>
                                    <span slot="title">疫情记录</span>
                                </el-menu-item>
                            </a>
                        </el-menu>
                    </el-col>
                </el-row>
            </el-aside>
            <el-container>
                <el-main>
                    <div v-if="!isShow" style="text-align: center;">
                        <el-col>
                            <h4>简介</h4>
                            <p>
                                为了进一步加强我馆的传染病疫情报告管理, 提高报告的效率和质量,
                                为疫情预防控制提供及时、准确的监测信息,制定了疫情管理系统。
                                <br>
                                此系统为简单的疫情管理系统，
                                用户可为自己的健康状况申报，
                                反应自身的健康状况。
                            </p>
                        </el-col>
                    </div>
                    <div v-if="isShow">
                        <iframe name="main" width="100%" height="600px" scrolling="no" frameborder="0"
                                marginheight="0" marginwidth="0"></iframe>
                    </div>
                </el-main>
                <el-footer>@by叶志铙，陈杰，李升典，罗小龙，蔡泽钦</el-footer>
            </el-container>
        </el-container>
    </el-container>

</div>
</body>
<!-- 一定要在底下才能识别 -->
<script src="/js/AdminMain.js"></script>


</html>
