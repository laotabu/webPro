<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/6/28
  Time: 16:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <title>showData</title>
    <meta charset="UTF-8">
    <!-- import CSS -->
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
    <link rel="stylesheet" href="/css/allUserTable.css">
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</head>

<body>
<div id="showData">
    <el-col class="topTable">
        <el-button type="primary" round  @click="addUser">新增+</el-button>
        <el-input  class="search" v-model="keyword" @keyup.enter="searchPerson" placeholder="请输入内容" style="width:300px" clearable>
            <el-button type="primary" @click="searchPerson" slot="append" icon="el-icon-search">搜索</el-button>
        </el-input>
    </el-col>

    <template>
        <el-table :data="tableData" height="450" border style="width: 100%" header-row-class-name="center">
            <el-table-column fixed prop="username" label="用户名" width="120">
            </el-table-column>
            <%--            <el-table-column  prop="password" label="密码" width="150">--%>
            <%--            </el-table-column>--%>
            <el-table-column prop="idNumber" label="身份证号码" width="150">
            </el-table-column>
            <el-table-column prop="realName" label="真实姓名" width="120">
            </el-table-column>
            <el-table-column prop="sex" label="性别" width="100">
            </el-table-column>
            <el-table-column prop="birthday" label="出生如期" width="120">
            </el-table-column>
            <el-table-column prop="phone" label="电话号码" width="120">
            </el-table-column>
            <el-table-column prop="address" label="地址" width="332">
            </el-table-column>

            <el-table-column
                    fixed="right"
                    label="操作"
                    width="200">
                <template slot-scope="scope">
                    <el-row>
                        <%--                        @click="details(scope.row.uid)"--%>
                        <el-button @click="updateUser(scope.row.uid)" type="primary" icon="el-icon-edit"
                                   circle></el-button>


                        <el-button @click="deleteUser(scope.row.uid)" type="danger" icon="el-icon-delete"
                                   circle></el-button>
                    </el-row>
                </template>
            </el-table-column>
        </el-table>

    </template>

    <el-dialog title="修改用户信息" :visible.sync="FormVisible1">
        <el-col>
            <el-form ref="updateForm" :model="updateForm" :label-width="formLabelWidth">
                <el-form-item label="用户名">
                    <el-col>
                        <el-input v-model="updateForm.username" placeholder="请输入身份证号"></el-input>
                    </el-col>
                </el-form-item>
                <el-form-item label="密码">
                    <el-col>
                        <el-input v-model="updateForm.password" placeholder="请输入身份证号"></el-input>
                    </el-col>
                </el-form-item>

                <el-form-item label="身份证号">
                    <el-col>
                        <el-input v-model="updateForm.idNumber" placeholder="请输入身份证号"></el-input>
                    </el-col>
                </el-form-item>
                <el-form-item label="真实姓名">
                    <el-col>
                        <el-input v-model="updateForm.realName" placeholder="请输入真实姓名"></el-input>
                    </el-col>
                </el-form-item>
                <el-form-item label="性别">
                    <el-radio-group v-model="updateForm.sex">
                        <el-radio label="男"></el-radio>
                        <el-radio label="女"></el-radio>
                    </el-radio-group>
                </el-form-item>
                <el-form-item label="出生日期">
                    <el-col>
                        <el-date-picker type="date" placeholder="输入出生日期" v-model="updateForm.birthday"
                                        style="width: 100%;">
                        </el-date-picker>
                    </el-col>
                </el-form-item>
                <el-form-item label="电话号码">
                    <el-col>
                        <el-input v-model="updateForm.phone" placeholder="请输入电话号码"></el-input>
                    </el-col>
                </el-form-item>
                <el-form-item label="地址">
                    <el-col>
                        <el-input v-model="updateForm.address" placeholder="请输入地址"></el-input>
                    </el-col>
                </el-form-item>
            </el-form>
        </el-col>
        <div slot="footer" class="dialog-footer">
            <el-button @click="FormVisible1 = false">取 消</el-button>
            <el-button type="primary" @click="updateUserDeal">确 定</el-button>
        </div>
    </el-dialog>

    <el-dialog title="新增用户信息" :visible.sync="FormVisible2">
        <el-form ref="addForm" :model="addForm" :label-width="formLabelWidth">
            <el-form-item label="用户名">
                <el-col>
                    <el-input v-model="addForm.username" placeholder="请输入身份证号"></el-input>
                </el-col>
            </el-form-item>
            <el-form-item label="密码">
                <el-col>
                    <el-input type="password" v-model="addForm.password" placeholder="请输入身份证号"></el-input>
                </el-col>
            </el-form-item>

            <el-form-item label="身份证号">
                <el-col>
                    <el-input v-model="addForm.idNumber" placeholder="请输入身份证号"></el-input>
                </el-col>
            </el-form-item>
            <el-form-item label="真实姓名">
                <el-col>
                    <el-input v-model="addForm.realName" placeholder="请输入真实姓名"></el-input>
                </el-col>
            </el-form-item>
            <el-form-item label="性别">
                <el-radio-group v-model="addForm.sex">
                    <el-radio label="男"></el-radio>
                    <el-radio label="女"></el-radio>
                </el-radio-group>
            </el-form-item>
            <el-form-item label="出生日期">
                <el-col>
                    <el-date-picker type="date" placeholder="输入出生日期" v-model="addForm.birthday"
                                    style="width: 100%;">
                    </el-date-picker>
                </el-col>
            </el-form-item>
            <el-form-item label="电话号码">
                <el-col>
                    <el-input v-model="addForm.phone" placeholder="请输入电话号码"></el-input>
                </el-col>
            </el-form-item>
            <el-form-item label="地址">
                <el-col>
                    <el-input v-model="addForm.address" placeholder="请输入地址"></el-input>
                </el-col>
            </el-form-item>
        </el-form>
        <div slot="footer" class="dialog-footer">
            <el-button @click="FormVisible2 = false">取 消</el-button>
            <el-button type="primary" @click="addUserDeal">确 定</el-button>
        </div>
    </el-dialog>

    <el-dialog
            title="删除用户"
            :visible.sync="FormVisible3"
            width="30%">
        <span>确认删除吗？</span>
        <span slot="footer" class="dialog-footer">
    <el-button @click="FormVisible3 = false">取 消</el-button>
    <el-button type="primary" @click="deleteUserDeal">确 定</el-button>
  </span>
    </el-dialog>

</div>
</body>
<!-- import Vue before Element -->
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<!-- import JavaScript -->
<script src="https://unpkg.com/element-ui/lib/index.js"></script>
<script src="/js/allUserTable.js"></script>
</html>


