<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/6/24
  Time: 12:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>个人信息</title>
</head>
<!-- 导入开发版本的Vue.js
创建Vue实例对象,设置el属性和data属性
使用简洁的模板语法把数据渲染到页面上 -->

<!-- 开发环境版本，包含了有帮助的命令行警告 -->
<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<!-- import Vue before Element -->
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<!-- import JavaScript -->
<script src="https://unpkg.com/element-ui/lib/index.js"></script>
<link rel="stylesheet" href="/css/personInformation.css">

<body>
<div id="personInformation" class="myFrom">
    <el-col :span="10">
        <el-form  ref="form" :model="form" label-width="80px">

            <el-form-item label="身份证号">
                <el-col>
                    <el-input v-model="form.idNumber" placeholder="请输入身份证号"></el-input>
                </el-col>


            </el-form-item>
            <el-form-item label="真实姓名">
                <el-col>
                    <el-input v-model="form.realName" placeholder="请输入真实姓名"></el-input>
                </el-col>
            </el-form-item>
            <el-form-item label="性别">
                <el-radio-group v-model="form.sex">
                    <el-radio label="男"></el-radio>
                    <el-radio label="女"></el-radio>
                </el-radio-group>
            </el-form-item>
            <el-form-item label="出生日期">
                <el-col>
                    <el-date-picker type="date" placeholder="输入出生日期" v-model="form.birthday" style="width: 100%;">
                    </el-date-picker>
                </el-col>
            </el-form-item>
            <el-form-item label="电话号码">
                <el-col>
                    <el-input v-model="form.phone" placeholder="请输入电话号码"></el-input>
                </el-col>
            </el-form-item>
            <el-form-item label="地址">
                <el-col>
                    <el-input v-model="form.address" placeholder="请输入地址"></el-input>
                </el-col>
            </el-form-item>
            <el-form-item>
               <!-- <a href="/user/" target="_parent">-->
                    <el-button type="primary" :plain="true" @click.native="onSubmit">保存</el-button>
                <!--</a>-->
                <a href="/user/" target="_parent">
                    <el-button type="reset">取消</el-button>
                </a>

            </el-form-item>
        </el-form>
    </el-col>
    <el-col :span="6" :offset="2">
        <el-upload class="avatar-uploader"
                   action="upload"
                   name="imgFile"
                   :show-file-list="false"
                   :on-success="handleAvatarSuccess"
                   :before-upload="beforeAvatarUpload">
            <img v-if="form.userImage" :src="'/userImages/'+form.userImage" class="avatar">
            <i v-else class="el-icon-plus avatar-uploader-icon"></i>
        </el-upload>
        点击上传图片
    </el-col>

</div>
<script src="/js/personInformation.js"></script>

</body>

</html>